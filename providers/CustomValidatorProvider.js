'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class CustomValidatorProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    // Register custom validator
    const Validator = use('Validator')
    Validator.extend('exist', this.isExist)
    Validator.extend('is-available', this.isAvailable)
  }

  async isExist(data, field, message, args, get) {
    const Database = use('Database')
    const value = get(data, field)
    try {
      const [table, column] = args
      const row = await Database.table(table).where(column, value).first()

      if (!row) {
        throw message
      }
    } catch (error) {
      throw message
    }
  }

  async isAvailable(data, field, message, args, get) {
    const Room = use('App/Models/Room')
    const value = get(data, field)
    try {
      const room = await Room.findBy('name', value)

      if (!room.is_available) {
        throw message
      }
    } catch (error) {
      throw message
    }
  }
}

module.exports = CustomValidatorProvider
