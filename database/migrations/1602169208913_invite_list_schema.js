'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InviteListSchema extends Schema {
  up() {
    this.create('invite_lists', (table) => {
      table.increments('id').primary()
      table.string('email').unique()
      table.string('role')
      table.string('token')
      table.string('expired_at')
      table.boolean('is_active').defaultTo(0)
      table.timestamps()
    })
  }

  down() {
    this.drop('invite_lists')
  }
}

module.exports = InviteListSchema
