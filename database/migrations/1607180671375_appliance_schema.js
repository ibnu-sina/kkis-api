'use strict'

const Schema = use('Schema')

class ApplianceSchema extends Schema {
  up() {
    this.create('appliances', (table) => {
      table.increments('id').primary()
      table.string('name')
      table.decimal('price')
      table.timestamps()
    })
  }

  down() {
    this.drop('appliances')
  }
}

module.exports = ApplianceSchema
