'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CourseSchema extends Schema {
  up() {
    this.create('courses', (table) => {
      table.increments('id').primary()
      table.string('name').notNullable()
      table.string('code').notNullable()
      table.string('description')
      table.boolean('is_checkin').defaultTo(0)
      table.boolean('is_checkout').defaultTo(0)
      table.timestamps()
    })
  }

  down() {
    this.drop('courses')
  }
}

module.exports = CourseSchema
