'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MeritSchema extends Schema {
  up() {
    this.create('merits', (table) => {
      table.increments('id').primary()
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.string('type')
      table.string('name')
      table.date('date')
      table.string('term')
      table.string('position')
      table.string('level')
      table.string('achievement')
      table.integer('merit')
      table.boolean('is_approved').defaultTo(false)
      table.string('session')
      table.integer('attachment_id').unsigned().references('id').inTable('student_uploads')
      table.timestamps()
    })
  }

  down() {
    this.drop('merits')
  }
}

module.exports = MeritSchema
