'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GuardianSchema extends Schema {
  up() {
    this.create('guardians', (table) => {
      table.increments('id').primary()
      table
        .integer('student_id')
        .unsigned()
        .references('id')
        .inTable('students')
      table.string('full_name', 254)
      table.string('phone_no', 80)
      table.timestamps()
    })
  }

  down() {
    this.drop('guardians')
  }
}

module.exports = GuardianSchema
