'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentAppliancesSchema extends Schema {
  up() {
    this.create('student_appliances', (table) => {
      table.increments('id').primary()
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.integer('appliance_id').unsigned().references('id').inTable('appliances')
      table.string('serial_no')
      table.timestamps()
    })
  }

  down() {
    this.drop('student_appliances')
  }
}

module.exports = StudentAppliancesSchema
