'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StayRequestSchema extends Schema {
  up() {
    this.create('stay_requests', (table) => {
      table.increments('id').primary()
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.date('start_date').notNullable()
      table.date('end_date').notNullable()
      table.integer('total_days')
      table.string('reason')
      table.integer('payment_method')
      table.string('status')
      table.decimal('price_per_day')
      table.integer('respond_admin_id').unsigned().references('id').inTable('admins')
      table.integer('paid_admin_id').unsigned().references('id').inTable('admins')
      table.datetime('respond_date')
      table.datetime('paid_date')
      table.datetime('checkin_date')
      table.datetime('checkout_date')
      table.timestamps()
    })
  }

  down() {
    this.drop('stay_requests')
  }
}

module.exports = StayRequestSchema
