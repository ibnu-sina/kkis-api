'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentActivitySchema extends Schema {
  up() {
    this.create('student_activities', (table) => {
      table.increments('id').primary()
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.integer('admin_id').unsigned().references('id').inTable('admins')
      table.string('status')
      table.string('type')
      table.timestamps()
    })
  }

  down() {
    this.drop('student_activities')
  }
}

module.exports = StudentActivitySchema
