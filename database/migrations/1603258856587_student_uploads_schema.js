'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentUploadsSchema extends Schema {
  up() {
    this.create('student_uploads', (table) => {
      table.increments('id').primary()
      table.string('type')
      table.string('path')
      table.string('url')
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.timestamps()
    })
  }

  down() {
    this.drop('student_uploads')
  }
}

module.exports = StudentUploadsSchema
