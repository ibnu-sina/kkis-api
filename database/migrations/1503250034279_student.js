'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentSchema extends Schema {
  up() {
    this.create('students', (table) => {
      table.increments('id').primary()
      table.string('name')
      table.string('email')
      table.string('password')
      table.string('ic_no').unique()
      table.string('religion')
      table.string('race')
      table.string('gender')
      table.date('birth_date')
      table.string('birth_place')
      table.string('matric_no').unique()
      table.string('phone_no')
      table.string('room_status')
      table.integer('course_id').unsigned().references('id').inTable('courses')
      table.integer('room_id').unsigned().references('id').inTable('rooms').onDelete('set null')
      table.timestamps()
    })
  }

  down() {
    this.drop('students')
  }
}

module.exports = StudentSchema
