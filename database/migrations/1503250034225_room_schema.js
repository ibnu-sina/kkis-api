'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoomSchema extends Schema {
  up() {
    this.create('rooms', (table) => {
      table.increments('id').primary()
      table.string('name').notNullable()
      table.string('block').notNullable()
      table.integer('number').notNullable()
      table.boolean('is_available').notNullable()
      table.integer('occupied').defaultTo(0)
      table.integer('capacity').notNullable()
      table.string('description')
      table.timestamps()
    })
  }

  down() {
    this.drop('rooms')
  }
}

module.exports = RoomSchema
