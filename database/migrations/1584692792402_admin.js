'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdminSchema extends Schema {
  up() {
    this.create('admins', (table) => {
      table.increments('id').primary()
      table.string('role')
      table.string('email').unique()
      table.string('name')
      table.string('gender')
      table.string('phone_no')
      table.string('password')
      table.timestamps()
    })
  }

  down() {
    this.drop('admins')
  }
}

module.exports = AdminSchema
