'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InventoryStorageSchema extends Schema {
  up() {
    this.create('inventory_storages', (table) => {
      table.increments('id').primary()
      table.string('status')
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.integer('request_box_no')
      table.string('request_description')
      table.datetime('request_date')
      table.datetime('request_resolve_date')
      table.integer('request_admin_id').unsigned().references('id').inTable('admins')
      table.integer('request_storekeeper_id').unsigned().references('id').inTable('admins')
      table.integer('claim_box_no')
      table.string('claim_description')
      table.datetime('claim_date')
      table.integer('claim_admin_id').unsigned().references('id').inTable('admins')
      table.string('claim_claimer_name')
      table.datetime('updated_at')
    })
  }

  down() {
    this.drop('inventory_storages')
  }
}

module.exports = InventoryStorageSchema
