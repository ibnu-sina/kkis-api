'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentInviteListSchema extends Schema {
  up() {
    this.create('student_invite_lists', (table) => {
      table.increments('id').primary()
      table.string('name')
      table.string('email').unique()
      table.string('matric_no')
      table.integer('course_id').unsigned().references('id').inTable('courses').onDelete('set null')
      table.integer('room_id').unsigned().references('id').inTable('rooms').onDelete('cascade')
      table.string('token')
      table.string('expired_at')
      table.boolean('is_active').defaultTo(0)
      table.timestamps()
    })
  }

  down() {
    this.drop('student_invite_lists')
  }
}

module.exports = StudentInviteListSchema
