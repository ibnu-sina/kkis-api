'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddressSchema extends Schema {
  up() {
    this.create('addresses', (table) => {
      table.increments('id').primary()
      table
        .integer('student_id')
        .unsigned()
        .references('id')
        .inTable('students')
      table.string('address_1', 254)
      table.string('address_2', 254)
      table.string('city', 254)
      table.string('state', 254)
      table.string('posscode', 254)
      table.string('country', 254)
      table.timestamps()
    })
  }

  down() {
    this.drop('addresses')
  }
}

module.exports = AddressSchema
