'use strict'

/*
|--------------------------------------------------------------------------
| InitSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Env = use('Env')
const StudentInviteList = use('App/Models/StudentInviteList')
const InviteList = use('App/Models/InviteList')
const Course = use('App/Models/Course')
const Room = use('App/Models/Room')

class InitSeeder {
  async run() {
    // Create room
    await Room.findOrCreate({
      block: 'A',
      number: 102,
      capacity: 2,
    })

    // Create course
    await Course.findOrCreate({
      name: 'MBBS3.2',
      code: 'mbbs_3.2',
    })

    // Invite student 1
    await StudentInviteList.findOrCreate({
      name: 'Student 1',
      email: 'irham@yopmail.com',
      matric_no: 'KKIS0001',
      course_id: 1,
      room_id: 1,
    })

    // Invite student 2
    await StudentInviteList.findOrCreate({
      name: 'Student 2',
      email: 'juaaa@yopmail.com',
      matric_no: 'KKIS0002',
      course_id: 1,
      room_id: 1,
    })

    // Invite admin 1
    await InviteList.findOrCreate({
      role: 'Developer',
      email: 'afrieee@yopmail.com',
    })

    // Invite admin 2
    await InviteList.findOrCreate({
      role: 'Developer',
      email: 'nazurahasfan@yopmail.com',
    })
  }
}

module.exports = InitSeeder
