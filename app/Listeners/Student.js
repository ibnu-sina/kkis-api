'use strict'
const Mail = use('Mail')
const Env = use('Env')
const moment = require('moment')

const Student = (exports = module.exports = {})

Student.newInvite = async (user) => {
  const student_url = Env.get('STUDENT_URL')
  const invite_link = `${student_url}/register/${user.token}`
  const expired_date = moment(user.expired_at).format('DD/MM/YYYY')
  const course = await user.course().fetch()
  const room = await user.room().fetch()

  try {
    await Mail.send(
      'emails.student-invite',
      { user, course, room, expired_date, invite_link },
      (message) => {
        message
          .to(user.email)
          .from(Env.get('MAIL_USERNAME'), Env.get('MAIL_NAME'))
          .subject("Congratulations! You're accepted.")
      }
    )
  } catch (error) {
    console.error(error)
  }
}

Student.courseStatusOpen = async ({ course, action }) => {
  try {
    const students = await course.students().fetch()
    const student_url = Env.get('STUDENT_URL')
    const action_name = action === 'checkin' ? 'Check-In' : 'Check-Out'
    const course_name = course.name

    students.toJSON().forEach(async (student) => {
      try {
        await Mail.send(
          'emails.course-status',
          { course_name, action_name, student_url },
          (message) => {
            message
              .to(student.email)
              .from(Env.get('MAIL_USERNAME'), Env.get('MAIL_NAME'))
              .subject(`KKIS: You can ${action_name} now`)
          }
        )
      } catch (error) {
        console.error(error)
      }
    })
  } catch (error) {
    console.log(error)
  }
}

Student.stayRequestUpdate = async ({ action, stayRequest }) => {
  const getPaymentMethod = (payment_method) => {
    if (payment_method === 1) return 'Cash'
    if (payment_method === 2) return 'Potongan Yuran'
  }

  const formatDate = (date) => moment(date).format('DD MMM YYYY')
  const student = await stayRequest.student().first()

  const getRespondId = (action) => {
    if (action === 'approved') return 1
    if (action === 'rejected') return 2
  }
  const email_data = {
    respond_id: getRespondId(action),
    respond: action,
    reason: stayRequest.reason,
    start_date: formatDate(stayRequest.start_date),
    end_date: formatDate(stayRequest.end_date),
    payment_method: getPaymentMethod(stayRequest.payment_method),
    price_per_day: 'RM' + stayRequest.price_per_day.toFixed(2),
    total_price: 'RM' + (stayRequest.total_days * stayRequest.price_per_day).toFixed(2),
    student_name: student.name,
  }

  try {
    await Mail.send('emails.stay-request-status', email_data, (message) => {
      message
        .to(student.email)
        .from(Env.get('MAIL_USERNAME'), Env.get('MAIL_NAME'))
        .subject('Respond: Permohonan Tinggal Semasa Cuti')
    })
  } catch (error) {
    console.log(error)
  }
}
