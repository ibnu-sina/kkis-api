'use strict'
const Mail = use('Mail')
const Env = use('Env')

const Admin = (exports = module.exports = {})

Admin.newInvite = async ({ user, invitor }) => {
  const { email, role, token } = user
  const admin_url = Env.get('ADMIN_URL')
  const invite_link = `${admin_url}/register/${token}`
  try {
    await Mail.send(
      'emails.admin-invite',
      { email, role, token, invitor, invite_link },
      (message) => {
        message
          .to(email)
          .from(Env.get('MAIL_USERNAME'), Env.get('MAIL_NAME'))
          .subject(`${invitor.name} has invited you as a KKIS ${role} admin`)
      }
    )
  } catch (error) {
    console.error(error)
  }
}
