'use strict'
const Mail = use('Mail')
const Env = use('Env')

const Auth = (exports = module.exports = {})

Auth.forgotPassword = async ({ user, token, user_type }) => {
  const client_url = Env.get(`${user_type.toUpperCase()}_URL`)
  const reset_link = `${client_url}/update-password/${token}`
  try {
    await Mail.send('emails.forgot-password', { user, reset_link }, (message) => {
      message
        .to(user.email)
        .from(Env.get('MAIL_USERNAME'), Env.get('MAIL_NAME'))
        .subject('Request for password change')
    })
  } catch (error) {
    console.error(error)
  }
}
