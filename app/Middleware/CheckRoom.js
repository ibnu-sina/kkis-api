'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
const Room = use('App/Models/Room')

class CheckRoom {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, response }, next) {
    const { room_id } = request.post()

    // Validate if room exist
    const room = await Room.find(room_id)
    if (!room) return response.notFound({ message: 'Room not found' })

    // Check room availabiliy
    if (!room.toJSON().is_available) {
      return response.badRequest({ message: `Room ${room.block + room.number} is full` })
    }

    request.body.room = room

    await next()
  }
}

module.exports = CheckRoom
