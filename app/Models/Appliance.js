'use strict'

const Model = use('Model')

class Appliance extends Model {
  static get hidden() {
    return ['created_at', 'updated_at']
  }

  students() {
    return this.belongsToMany('App/Models/Student').pivotTable('student_appliances')
  }
}

module.exports = Appliance
