'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudentUpload extends Model {
  student() {
    return this.belongsTo('App/Models/Student')
  }

  merit() {
    return this.hasMany('App/Models/Merit', 'id', 'attachment_id')
  }
}

module.exports = StudentUpload
