'use strict'

const Model = use('Model')

class StayRequest extends Model {
  student() {
    return this.belongsTo('App/Models/Student')
  }

  respond_admin() {
    return this.belongsTo('App/Models/Admin', 'respond_admin_id', 'id')
  }

  paid_admin() {
    return this.belongsTo('App/Models/Admin', 'paid_admin_id', 'id')
  }
}

module.exports = StayRequest
