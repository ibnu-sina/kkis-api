'use strict'

const Model = use('Model')
const Event = use('Event')

const { generateToken } = use('App/Helpers')

class InviteList extends Model {
  static boot() {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      const { token, expired_at } = await generateToken(1)
      userInstance.token = token
      userInstance.expired_at = expired_at
    })
  }
}

module.exports = InviteList
