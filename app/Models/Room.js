'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Student = use('App/Models/Student')

class Room extends Model {
  static boot() {
    super.boot()
    this.addHook('beforeSave', async (room) => {
      // Capitalize room block
      room.block = room.block.toUpperCase()

      // Update room name
      room.name = room.block + room.number

      // Update room's occupied
      if (room.id) {
        const students = await Student.query().where({ room_id: room.id }).getCount()
        room.occupied = students
      }

      // Update is_available
      room.is_available = room.occupied ? room.occupied < room.capacity : 1
    })
  }

  students() {
    return this.hasMany('App/Models/Student')
  }
}

module.exports = Room
