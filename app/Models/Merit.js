'use strict'

const Model = use('Model')

class Merit extends Model {
  student() {
    return this.belongsTo('App/Models/Student')
  }

  attachment() {
    return this.belongsTo('App/Models/StudentUpload', 'attachment_id', 'id')
  }
}

module.exports = Merit
