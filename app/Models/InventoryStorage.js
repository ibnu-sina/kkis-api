'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class InventoryStorage extends Model {
  static get createdAtColumn() {
    return 'request_date'
  }
  static get updatedAtColumn() {
    return 'updated_at'
  }

  request_admin() {
    return this.belongsTo('App/Models/Admin', 'request_admin_id', 'id')
  }

  claim_admin() {
    return this.belongsTo('App/Models/Admin', 'claim_admin_id', 'id')
  }

  student() {
    return this.belongsTo('App/Models/Student')
  }

  request_storekeeper() {
    return this.belongsTo('App/Models/Admin', 'request_storekeeper_id', 'id')
  }
}

module.exports = InventoryStorage
