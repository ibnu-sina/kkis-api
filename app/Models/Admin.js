'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')

class Admin extends Model {
  static boot() {
    super.boot()
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static get hidden() {
    return ['password']
  }

  tokens() {
    return this.hasMany('App/Models/AdminToken')
  }

  invited() {
    return this.hasOne('App/Models/InviteList')
  }

  async verifyPassword(password) {
    return await Hash.verify(password, this.password)
  }
}

module.exports = Admin
