'use strict'

const Model = use('Model')

class StudentActivity extends Model {
  student() {
    return this.belongsTo('App/Models/Student')
  }

  admin() {
    return this.belongsTo('App/Models/Admin')
  }
}

module.exports = StudentActivity
