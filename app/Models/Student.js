'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')

class Student extends Model {
  static boot() {
    super.boot()
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static get hidden() {
    return ['password']
  }

  async verifyPassword(password) {
    return await Hash.verify(password, this.password)
  }

  tokens() {
    return this.hasMany('App/Models/StudentToken')
  }

  guardian() {
    return this.hasOne('App/Models/Guardian')
  }

  address() {
    return this.hasOne('App/Models/Address')
  }

  course() {
    return this.belongsTo('App/Models/Course')
  }

  room() {
    return this.belongsTo('App/Models/Room')
  }

  merits() {
    return this.hasMany('App/Models/Merit')
  }

  uploads() {
    return this.hasMany('App/Models/StudentUpload')
  }

  stay_requests() {
    return this.hasMany('App/Models/StayRequest')
  }

  activities() {
    return this.hasMany('App/Models/StudentActivity')
  }

  appliances() {
    return this.belongsToMany('App/Models/Appliance')
      .withPivot(['serial_no'])
      .pivotTable('student_appliances')
  }
}

module.exports = Student
