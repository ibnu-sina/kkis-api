'use strict'

const Model = use('Model')
const Event = use('Event')

const { generateToken } = use('App/Helpers')

class StudentInviteList extends Model {
  static boot() {
    super.boot()

    this.addHook('beforeSave', async (userInstance) => {
      const { token, expired_at } = await generateToken(7)
      userInstance.token = token
      userInstance.expired_at = expired_at
    })

    this.addHook('afterSave', async (userInstance) => {
      Event.fire('new::student-invite', userInstance)
    })
  }

  course() {
    return this.belongsTo('App/Models/Course')
  }

  room() {
    return this.belongsTo('App/Models/Room')
  }
}

module.exports = StudentInviteList
