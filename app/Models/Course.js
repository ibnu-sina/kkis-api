'use strict'

const Model = use('Model')

class Course extends Model {
  students() {
    return this.hasMany('App/Models/Student')
  }

  invited_students() {
    return this.hasMany('App/Models/StudentInviteList')
  }
}

module.exports = Course
