'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Appliance = use('App/Models/Appliance')
const Student = use('App/Models/Student')
const Database = use('Database')
/**
 * Resourceful controller for interacting with appliances
 */
class ApplianceController {
  /**
   * Show a list of all appliances.
   * GET appliances
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    const { page, limit } = request.get()
    try {
      const appliances = await Appliance.query().paginate(page, limit)
      return response.ok({ ...appliances.toJSON() })
    } catch (error) {
      return response.internalServerError({
        error,
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
      })
    }
  }

  /**
   * Show a list of all student appliances.
   * GET appliances/pivot
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async indexPivot({ request, response }) {
    const { page, limit, q: searchQuery } = request.get()
    const fetchQuery = Database.from('student_appliances')
      .select(
        'student_appliances.id',
        'student_appliances.serial_no',
        'students.id as student_id',
        'students.name as student_name',
        'appliances.id as appliance_id',
        'appliances.name as appliance_name',
        'appliances.price as appliance_price'
      )
      .innerJoin('students', 'student_appliances.student_id', 'students.id')
      .innerJoin('appliances', 'student_appliances.appliance_id', 'appliances.id')

    // Add search query
    if (searchQuery) {
      const keyword = `%${searchQuery}%`

      fetchQuery.orWhere('serial_no', 'LIKE', keyword)
      fetchQuery.orWhere('appliances.name', 'LIKE', keyword)
      fetchQuery.orWhere('students.name', 'LIKE', keyword)
    }

    try {
      const student_appliances = await fetchQuery.paginate(page, limit)

      return response.ok(student_appliances)
    } catch (error) {
      return response.internalServerError({
        error,
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
      })
    }
  }

  /**
   * Show a list of all of my appliances.
   * GET appliances/me
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async me({ auth, response }) {
    const user = await auth.getUser()
    try {
      const appliances = await user.appliances().fetch()
      return response.ok({ data: appliances })
    } catch (error) {
      return response.internalServerError({
        error,
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
      })
    }
  }

  /**
   * Create/save a new appliance.
   * POST appliances
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { name, price } = request.post()

    // Check if empty
    const isEmpty = [name, price].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Name and price is required' })

    // Add to database
    try {
      const appliance = await Appliance.create({ name, price: Number(price).toFixed(2) })
      return response.created({ data: appliance, message: 'New appliance created' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Display a single appliance.
   * GET appliances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {}

  /**
   * Update appliance details.
   * PUT or PATCH appliances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { name, price } = request.post()

    // Check if empty
    const isEmpty = [name, price].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Name and price is required' })

    const appliance = await Appliance.find(params.id)
    if (!appliance) return response.notFound({ message: 'Appliance not found' })

    try {
      // Update to database
      appliance.name = name
      appliance.price = price
      await appliance.save()

      const updatedAppliance = await Appliance.find(params.id)
      return response.ok({ data: updatedAppliance, message: 'Appliance updated' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Delete a appliance with id.
   * DELETE appliances/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, response }) {
    const appliance = await Appliance.find(params.id)
    if (!appliance) return response.notFound({ message: 'Appliance not found' })

    // Cannot delete if attached to students
    const studentsCount = await appliance.students().getCount()
    if (studentsCount) {
      return response.badRequest({
        message: "Some student has registered this appliance, can't delete it.",
      })
    }

    try {
      await appliance.delete()
      return response.ok({ message: 'Delete success', data: appliance.id })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Detach an appliance from student.
   * DELETE appliances/:id/:student_id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async detach({ params, response }) {
    const student = await Student.find(params.student_id)
    if (!student) return response.badRequest({ message: 'Student not found' })

    const appliance = await Appliance.find(params.id)
    if (!appliance) return response.badRequest({ message: 'Appliance not found' })

    try {
      await student.appliances().detach([appliance.id])
      return response.ok({ message: 'Appliances removed from student' })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Add serial no. to student appliance.
   * PUT appliances/:id/:student_id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async attach({ params, request, response }) {
    const { student_id, id: appliance_id } = params
    const { serial_no } = request.post()

    // Check if serial_no exist
    if (!serial_no) return response.badRequest({ message: 'Serial number is required' })

    // Check if student appliance relationship is valid
    const appliance = await Database.from('student_appliances').where({ student_id, appliance_id })
    if (!appliance.length) {
      return response.badRequest({ message: 'Invalid appliance id or student id' })
    }

    try {
      // Add serial_no to student appliance
      const student = await Student.find(student_id)
      await student.appliances().detach([appliance_id])
      await student.appliances().attach(appliance_id, (row) => (row.serial_no = serial_no))
      return response.ok({ message: 'Serial no. added' })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Register appliances.
   * POST appliances/register
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async register({ auth, request, response }) {
    const { appliances } = request.post()
    if (!appliances || appliances.length === 0) {
      return response.badRequest({ message: 'Appliance id is required' })
    }

    const user = await auth.getUser()

    try {
      await user.appliances().attach(appliances)
      return response.ok({ message: 'Appliance registered successfully' })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
      })
    }
  }
}

module.exports = ApplianceController
