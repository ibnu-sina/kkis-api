'use strict'

/**
 * Resourceful controller for interacting with inventory-storages
 */
const InventoryStorage = use('App/Models/InventoryStorage')
class InventoryStorageController {
  /**
   * Show a list of all inventory-storages.
   * GET inventory-storages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    const { page, limit, status } = request.get()

    const fetchQuery = InventoryStorage.query()
      .with('student', (builder) => builder.select('id', 'name'))
      .with('request_admin', (builder) => builder.select('id', 'name'))
      .with('request_storekeeper', (builder) => builder.select('id', 'name'))
      .with('claim_admin', (builder) => builder.select('id', 'name'))

    // Add status filter
    if (status) {
      fetchQuery.orWhere({ status })
    }

    try {
      const storageRequest = await fetchQuery.paginate(page, limit)
      return response.ok(storageRequest)
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong' })
    }
  }

  /**
   * Show a list all of my inventory storages.
   * GET inventory-storages/me
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async mine({ auth, request, response }) {
    const { page, limit } = request.get()
    const student = await auth.getUser()
    try {
      const storageRequest = await InventoryStorage.query()
        .with('student', (builder) => builder.select('id', 'name'))
        .with('request_admin', (builder) => builder.select('id', 'name'))
        .with('request_storekeeper', (builder) => builder.select('id', 'name'))
        .with('claim_admin', (builder) => builder.select('id', 'name'))
        .where({ student_id: student.id })
        .paginate(page, limit)
      return response.ok(storageRequest.toJSON())
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong' })
    }
  }

  /**
   * Render a form to be used for creating a new inventorystorage.
   * GET inventory-storages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {}

  /**
   * Create/save a new inventorystorage.
   * POST inventory-storages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ auth, response }) {
    const student = await auth.getUser()

    try {
      const storage = await InventoryStorage.create({ student_id: student.id, status: 'pending' })
      return response.created({ data: storage, message: 'Request has been made successfully' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Resolve inventory storage pending request.
   * POST inventory-storages/:id/resolve
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async resolve({ auth, params, request, response }) {
    const { request_box_no, request_description, request_storekeeper_id } = request.post()

    // Check if required field exist
    const isEmpty = [request_box_no, request_storekeeper_id].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Box no. and storekeeper info required' })

    // Check if storage request exist
    const storageRequest = await InventoryStorage.find(params.id)
    if (!storageRequest) return response.badRequest({ message: 'Storage request not found' })

    // Update storage request info
    const admin = await auth.getUser()
    storageRequest.status = 'in_store'
    storageRequest.request_box_no = request_box_no
    storageRequest.request_description = request_description
    storageRequest.request_admin_id = admin.id
    storageRequest.request_storekeeper_id = request_storekeeper_id
    storageRequest.request_resolve_date = new Date()

    try {
      await storageRequest.save()
      return response.ok({ data: storageRequest, message: 'Storage request has been resolved' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Claim inventory storage in store request.
   * POST inventory-storages/:id/claim
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async claim({ auth, params, request, response }) {
    const { claim_box_no, claim_description, claim_claimer_name } = request.post()

    // Check if required field exist
    const isEmpty = [claim_box_no, claim_claimer_name].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Box no. and claimer name  required' })

    // Check if storage request exist
    const storageRequest = await InventoryStorage.find(params.id)
    if (!storageRequest) return response.badRequest({ message: 'Storage request not found' })

    // Update storage request info
    const admin = await auth.getUser()
    storageRequest.status = 'claimed'
    storageRequest.claim_box_no = claim_box_no
    storageRequest.claim_description = claim_description
    storageRequest.claim_admin_id = admin.id
    storageRequest.claim_claimer_name = claim_claimer_name
    storageRequest.claim_date = new Date()

    try {
      await storageRequest.save()
      return response.ok({ data: storageRequest, message: 'Storage request has been claim' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Display a single inventorystorage.
   * GET inventory-storages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {}

  /**
   * Render a form to update an existing inventorystorage.
   * GET inventory-storages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {}

  /**
   * Update inventorystorage details.
   * PUT or PATCH inventory-storages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a inventorystorage with id.
   * DELETE inventory-storages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ auth, params, response }) {
    // Check if storage request not found
    const storageRequest = await InventoryStorage.find(params.id)
    if (!storageRequest) return response.notFound({ message: 'Storage request not found' })

    // Check if request belong to student
    const student = await auth.getUser()
    if (storageRequest.student_id !== student.id) {
      return response.badRequest({ message: "Can't delete request that is not yours" })
    }

    // Check storage request status
    if (storageRequest.status !== 'pending') {
      return response.badRequest({ message: "Resolved request can't be deleted" })
    }

    // Delete storage request
    try {
      await storageRequest.delete()
      return response.ok({ message: 'Request deleted successfully' })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }
}

module.exports = InventoryStorageController
