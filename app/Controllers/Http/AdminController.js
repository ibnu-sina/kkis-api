'use strict'

const Admin = use('App/Models/Admin')
const AdminToken = use('App/Models/AdminToken')
const InviteList = use('App/Models/InviteList')

class AdminController {
  /**
   * Show a list of all admins.
   * GET admins
   *
   */
  async index({ request, response }) {
    const { q: searchQuery, page, limit } = request.get()

    const fetchQuery = Admin.query()

    // Search query if any
    if (searchQuery) {
      const keyword = `%${searchQuery}%`

      fetchQuery
        .orWhere('name', 'LIKE', keyword)
        .orWhere('email', 'LIKE', keyword)
        .orWhere('role', 'LIKE', keyword)
        .orWhere('phone_no', 'LIKE', keyword)
    }
    try {
      const admins = await fetchQuery.paginate(page, limit)
      return response.ok(admins)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Create/save a new admin.
   * POST admins
   *
   */
  async store({ request, response, auth }) {
    const { name, password, confirm_password, token } = request.post()

    // Check if token empty
    if (!token) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if token exist
    const invited = await InviteList.findBy({ token })
    if (!invited) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if token expired
    const isExpired = Date.now() > new Date(invited.expired_at)
    if (isExpired) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if password match
    if (password !== confirm_password) {
      return response.badRequest({ message: 'Password do not match' })
    }

    // Create admin
    const newAdmin = await Admin.create({
      name,
      password,
      role: invited.role,
      email: invited.email,
    })

    // Delete email from invite list
    await invited.delete()

    // Attempt login
    try {
      const data = await auth
        .authenticator('Admin')
        .withRefreshToken()
        .attempt(invited.email, password)
      return response.ok({ message: 'Register success', data })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Display a single admin.
   * GET admins/:id
   *
   */
  async show({ params, response }) {
    const data = await Admin.find(params.id)
    if (!data) return response.notFound({ message: 'Admin not found' })
    return response.ok({ data })
  }

  /**
   * Update my profile.
   * PUT or PATCH admins
   *
   */
  async update({ auth, request, response }) {
    const { name, phone_no, gender } = request.post()
    if (!name) return response.badRequest({ message: 'Name is required' })

    const user = await auth.getUser()

    // Update admin
    user.name = name ? name : user.name
    user.phone_no = phone_no ? phone_no : user.phone_no
    user.gender = gender ? gender : user.gender
    await user.save()

    return response.ok({ message: 'Profile updated', data: user })
  }

  /**
   * Delete a admin with id.
   * DELETE admins/:id
   *
   */
  async destroy({ params, response, auth }) {
    // Find if exist
    const admin = await Admin.find(params.id)
    if (!admin) return response.notFound({ message: 'Admin not found' })

    // Check user priviledge
    const user = await auth.getUser()
    if (user.id !== admin.id) {
      if (user.role === 'JTK') return response.badRequest({ message: 'Not allowed to delete' })
    }

    // Delete related tokens
    const allTokens = await AdminToken.all()
    const adminToken = allTokens.toJSON().map((i) => (i.admin_id === admin.id ? i : null))
    if (adminToken.length !== 0) {
      adminToken.forEach(async (i) => {
        if (i !== null) {
          const token = await AdminToken.find(i.id)
          await token.delete()
        }
      })
    }

    // Delete invite list admin
    const invited = await InviteList.findBy({ email: admin.email })
    await invited.delete()

    // Delete admin
    await admin.delete()

    return response.ok({ message: 'Delete success', data: admin.id })
  }
}

module.exports = AdminController
