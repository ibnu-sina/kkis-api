'use strict'

const Helpers = use('Helpers')
const { validateAll } = use('Validator')
const { Capitalize } = use('App/Helpers')

const StudentInviteList = use('App/Models/StudentInviteList')
const Course = use('App/Models/Course')
const Room = use('App/Models/Room')

const fs = Helpers.promisify(require('fs'))
const csv2json = require('csvjson-csv2json')
const path = require('path')
const uuid = require('uuid')

class StudentInviteListController {
  /**
   * GET student-invites
   * Show all student invite list.
   *
   */
  async index({ request, response }) {
    const { q: searchQuery, attach, page, limit, room_id, course_id } = request.get()

    const fetchQuery = StudentInviteList.query().orderBy('expired_at')

    // Attach any relationship
    if (attach) {
      if (attach.includes('course')) fetchQuery.with('course')
      if (attach.includes('room')) fetchQuery.with('room')
    }

    // Search query if any
    if (searchQuery) {
      const keyword = `%${searchQuery}%`
      fetchQuery
        .orWhere('name', 'LIKE', keyword)
        .orWhere('email', 'LIKE', keyword)
        .orWhere('matric_no', 'LIKE', keyword)
    }

    // Add room filter
    if (room_id) {
      fetchQuery.where({ room_id })
    }

    // Add course filter
    if (course_id) {
      fetchQuery.where({ course_id })
    }

    try {
      const inviteList = await fetchQuery.paginate(page, limit)
      return response.ok(inviteList)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * POST student-invites
   * Add a new student in invite list.
   *
   */
  async store({ request, response }) {
    const { name, email, matric_no, course_id, room_id } = request.post()

    // Check if required field exist
    const isEmpty = [name, email, matric_no, course_id, room_id].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Required fields is not completed' })

    const body = { name, email, matric_no, course_id, room_id }

    // Validate email and matric_no
    const rule = {
      email: 'unique:students,email|unique:student_invite_lists,email',
      matric_no: 'unique:students,matric_no|unique:student_invite_lists,matric_no',
    }
    const validation = await validateAll(body, rule)
    if (validation.fails()) {
      const message = validation
        .messages()
        .map((error) => Capitalize(error.field.split('_').join(' ')))
      return response.badRequest({ message: message.join(' and ') + ' already exist' })
    }

    // Get course info
    const course = await Course.find(course_id)
    if (!course) return response.badRequest({ message: 'Invalid course' })

    try {
      // Add to invite list
      const invited = await StudentInviteList.create(body)

      invited.course = await invited.course().fetch()
      invited.room = await invited.room().fetch()
      return response.created({ message: 'Email invited', data: invited })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * POST student-invites/multiple
   * Add a new student in invite list.
   *
   */
  async storeMultiple({ request, response }) {
    const { students } = request.post()

    // Validate if data is sent
    if (!students || students.length === 0) return response.badRequest({ message: 'No data sent' })

    // Validation rules
    const validationRule = {
      email: 'required|email|unique:students|unique:student_invite_lists',
      matric_no: 'required|unique:students|unique:student_invite_lists',
      course_code: 'required|exist:courses,code',
      room: 'required|exist:rooms,name|is-available',
    }

    // Validation message
    const validationMessage = {
      'email.required': 'Email is required',
      'matric_no.required': 'Matric No. is required',
      'course_code.required': 'Course is required',
      'room.required': 'Room is required',
      'email.email': 'Invalid email',
      'email.unique': 'Email already exist',
      'matric_no.unique': 'Matric No. already exist',
      'course_code.exist': 'Course does not exist',
      'room.exist': 'Room does not exist',
      'room.is-available': 'Room is full',
    }

    // Validate each student entry
    const labeledStudents = await Promise.all(
      students.map(async (student) => {
        // Validate student
        const validation = await validateAll(student, validationRule, validationMessage)

        // Get all validation error
        const errorMessages = validation.messages()

        // Return if no errors
        if (!errorMessages) return { ...student, errors: errorMessages }

        // Get first error for each column
        const firstErrors = [
          errorMessages.find((i) => i.field === 'email'),
          errorMessages.find((i) => i.field === 'matric_no'),
          errorMessages.find((i) => i.field === 'course_code'),
          errorMessages.find((i) => i.field === 'room'),
        ]

        // Remove in firstErrors
        const errors = firstErrors.filter((i) => i)
        return { ...student, errors }
      }),
    )

    // Return error if has errors
    const hasError = labeledStudents.some((i) => Boolean(i.errors))
    if (hasError) {
      return response.badRequest({
        data: labeledStudents,
        message: 'Please edit or remove all errors',
      })
    }

    // Get all course and room ids
    const courseIds = await Course.pair('code', 'id')
    const roomIds = await Room.pair('name', 'id')

    // Prepare data for bulk insert
    const sanitizedStudents = labeledStudents.map((student) => {
      return {
        name: student.name,
        email: student.email,
        matric_no: student.matric_no,
        course_id: courseIds[student.course_code],
        room_id: roomIds[student.room],
      }
    })

    // Save to db
    try {
      const invited = await StudentInviteList.createMany(sanitizedStudents)
      return response.created({ message: 'Students invited', data: invited })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong while saving database' })
    }
  }

  /**
   * POST student-invites/parse
   * Parse csv and return json
   *
   */
  async parse({ request, response }) {
    const file = request.file('file')
    const filePath = path.join(Helpers.tmpPath(), 'input.csv')

    // Move incoming file to tmp
    await file.move(Helpers.tmpPath(), { name: 'input.csv', overwrite: true })
    if (!file.moved()) {
      return response.badRequest({ message: 'Something went wrong', error: file.error() })
    }

    try {
      // Read csv data and convert to json
      const csv = await fs.readFile(filePath, 'utf8')
      const json = csv2json(csv, { parseNumber: true })

      // Remove file from tmp
      await fs.unlink(filePath)

      // Structure response data
      const data = json.map((i) => {
        return {
          id: uuid.v4(),
          name: i['Name'],
          email: i['Email'],
          matric_no: i['Matric No.'],
          course_code: i['Course code'],
          room: i['Room'],
        }
      })

      return response.ok({ data })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * GET student-invites/:token
   * Check for valid token
   *
   */
  async show({ params, response }) {
    const token = params.token
    const invited = await StudentInviteList.findBy({ token })

    // Check if exist
    if (!invited) return response.notFound({ message: 'Not in invite list' })

    // Check if active
    if (invited.is_active) return response.badRequest({ message: 'Account already activated' })

    // Check if expired
    const isExpired = Date.now() > new Date(invited.expired_at)
    if (isExpired)
      return response.badRequest({ message: 'Link has expired. Please ask for new invite.' })

    return response.ok({ data: invited })
  }

  /**
   * Renew student's invite link.
   * PUT student-invites/:id/renew
   *
   */
  async renew({ params, response }) {
    // Check if link exist
    const invited = await StudentInviteList.find(params.id)
    if (!invited) return response.notFound({ message: 'Invite link not found' })

    // Check if expired
    const isExpired = Date.now() > new Date(invited.expired_at)
    if (!isExpired) return response.badRequest({ message: 'Invite link is active' })

    // Reset expiry date and renew token
    try {
      await invited.save()
      return response.ok({ message: 'Invite link has been updated', data: invited })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong while renewing invite link',
      })
    }
  }

  /**
   * Delete a student invite list with id.
   * DELETE student-invites/:id
   *
   */
  async destroy({ params, response }) {
    const invited = await StudentInviteList.find(params.id)
    if (!invited) return response.notFound({ message: 'Invited email not found' })
    if (invited.is_active) return response.badRequest({ message: "Can't delete activated account" })

    try {
      await invited.delete()
      return response.ok({ message: 'Delete success', data: invited.id })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong ' })
    }
  }
}

module.exports = StudentInviteListController
