'use strict'

const StudentActivity = use('App/Models/StudentActivity')

class StudentActivityController {
  /**
   * Show a list of all student activities.
   * GET student-activities
   *
   */
  async index({ params, request, response }) {
    const type = params.type
    const { page, limit, status } = request.get()

    const fetchQuery = StudentActivity.query()
      .with('student', (builder) => {
        builder
          .select('id', 'name', 'room_id', 'matric_no', 'course_id')
          .with('course', (builder) => builder.select('id', 'name'))
          .with('room', (builder) => builder.select('id', 'name'))
      })
      .with('admin', (builder) => builder.select('id', 'name'))
      .where({ type })
      .where({ status })

    try {
      const activities = await fetchQuery.paginate(page, limit)
      return response.ok(activities)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Create/save a new studentactivity.
   * POST student-activities/:type
   *
   */
  async store({ auth, params, response }) {
    const { type } = params
    const user = await auth.getUser()
    const course = await user.course().fetch()

    const validActivity = ['checkin', 'checkout']

    // Check for type
    if (!type || !validActivity.includes(type)) {
      return response.badRequest({
        message: 'Invalid activity type',
        messageCode: 'invalid_activity',
      })
    }

    // Check if course activity is active
    let active
    if (type === 'checkin') active = course.is_checkin
    if (type === 'checkout') active = course.is_checkout

    if (!active) {
      return response.badRequest({
        message: `Students of ${course.name} is not allowed to ${type}`,
        messageCode: 'activity_request_fail',
      })
    }

    // Check for valid room_status for activity request
    const { room_status } = user
    if (type === 'checkin' && room_status === 'checked_in') {
      return response.badRequest({
        message: "You're already checked in",
        messageCode: 'invalid_room_status',
      })
    }
    if (type === 'checkout' && room_status === 'checked_out') {
      return response.badRequest({
        message: "You're already checked out",
        messageCode: 'invalid_room_status',
      })
    }

    // Check for duplicate request
    const pendingActivity = await user.activities().where({ type, status: 'pending' }).first()
    if (pendingActivity) {
      return response.badRequest({
        message: 'Request already made',
        messageCode: 'duplicate_activity',
      })
    }

    // Create request
    const activityRequest = await StudentActivity.create({
      student_id: user.id,
      status: 'pending',
      type,
    })

    return response.ok({
      data: activityRequest,
      message: 'Request has been made',
      messageCode: 'activity_request_success',
    })
  }

  /**
   * Display a single studentactivity.
   * GET student-activities/:id
   *
   */
  async show({ params, request, response }) {}

  /**
   * Update studentactivity details.
   * PUT or PATCH student-activities/:id/:action
   *
   */
  async update({ auth, params, response }) {
    const { id, action } = params
    const user = await auth.getUser()

    const activity = await StudentActivity.find(id)

    // Check if exist
    if (!activity) {
      return response.badRequest({
        message: 'Activity not found',
        messageCode: 'invalid_activity',
      })
    }

    // Check for valid action
    const validAction = ['approved', 'rejected']
    if (!validAction.includes(action)) {
      return response.badRequest({
        message: 'Invalid action ',
        messageCode: 'invalid_action_activity',
      })
    }

    try {
      // Update activity
      activity.admin_id = user.id
      activity.status = action
      await activity.save()

      // Update student room_status
      const student = await activity.student().fetch()
      if (activity.type === 'checkin') {
        student.room_status = 'checked_in'
      }
      if (activity.type === 'checkout') {
        student.room_status = 'checked_out'
      }
      await student.save()

      const updatedActivity = await StudentActivity.query()
        .where({ id })
        .with('student', (builder) => {
          builder
            .select('id', 'name', 'room_id', 'matric_no', 'course_id')
            .with('course', (builder) => builder.select('id', 'name'))
            .with('room', (builder) => builder.select('id', 'name'))
        })
        .with('admin', (builder) => builder.select('id', 'name'))
        .first()

      return response.ok({
        message: 'Activity updated',
        messageCode: 'update_activity_success',
        data: updatedActivity,
      })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
        error,
      })
    }
  }

  /**
   * Delete a studentactivity with id.
   * DELETE student-activities/:id
   *
   */
  async destroy({ params, request, response }) {}
}

module.exports = StudentActivityController
