'use strict'

const Event = use('Event')
const InviteList = use('App/Models/InviteList')
const { validate } = use('Validator')

class InviteListController {
  /**
   * Check for valid token
   * GET invite/:token
   *
   */
  async show({ params, response }) {
    const token = params.token
    const invited = await InviteList.findBy({ token })

    // Check if exist
    if (!invited) return response.notFound({ message: 'Not in invite list' })

    // Check if active
    if (invited.is_active) return response.notFound({ message: 'Account already activated' })

    // Check if expired
    const isExpired = Date.now() > new Date(invited.expired_at)
    if (isExpired)
      return response.badRequest({ message: 'Link has expired. Please ask for new invite.' })

    return response.ok({ data: invited })
  }

  /**
   * Show all invite list.
   * GET invite
   *
   */
  async index({ request, response }) {
    const { q: searchQuery, page, limit } = request.get()

    const fetchQuery = InviteList.query().orderBy('expired_at')

    // Search query if any
    if (searchQuery) {
      const keyword = `%${searchQuery}%`
      fetchQuery.orWhere('email', 'LIKE', keyword).orWhere('role', 'LIKE', keyword)
    }

    try {
      const admins = await fetchQuery.paginate(page, limit)
      return response.ok(admins)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Delete an invite list with id
   * DELETE invite/:id
   *
   */
  async destroy({ params, response }) {
    const invited = await InviteList.find(params.id)
    if (!invited) return response.notFound({ message: 'Invited email not found' })
    if (invited.is_active) return response.badRequest({ message: "Can't delete activated account" })
    await invited.delete()

    return response.ok({ message: 'Delete success', data: invited.id })
  }

  /**
   * Add email to invite list 
   * POST invite
   *
   */
  async store({ request, response, auth }) {
    const { email, role } = request.post()

    // Get invitor info
    const invitor = await auth.getUser()

    // Check if email and role exist
    const isEmpty = [email, role].some((i) => !i)
    if (isEmpty) return response.badRequest({ message: 'Email and role is required' })

    const body = { email, role }

    // Validate unique email
    const rule = { email: 'unique:admins,email|unique:invite_lists,email' }
    const validation = await validate(body, rule)
    if (validation.fails()) {
      return response.badRequest({ message: 'Email already invited/active' })
    }

    try {
      // Add to invite list
      const invited = await InviteList.create(body)

      // Fire new invite
      Event.fire('new::invite', { user: invited, invitor })
      return response.created({ message: 'Email invited', data: invited })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Renew invite link
   * PUT invite/:id/renew
   *
   */
  async renew({ auth, params, response }) {

    // Check if already invited
    const invited = await InviteList.find(params.id)
    if (!invited) return response.notFound({ message: 'Invitation link not found' })

    // Reset expiry date and renew token
    invited.save()
    
    // Get renewer info
    const invitor = auth.getUser()

    // Fire new invite
    Event.fire('new::invite', { user: invited, invitor })
    return response.ok({ message: 'Invite link has been updated', data: invited })
  }
}

module.exports = InviteListController
