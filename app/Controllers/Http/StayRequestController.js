'use strict'

const StayRequest = use('App/Models/StayRequest')
const Event = use('Event')
const moment = require('moment')
class StayRequestController {
  /**
   * Show a list of all stay-requests.
   * GET stay-requests
   *
   */
  async index({ request, response }) {
    const { page, limit, status } = request.get()
    const fetchQuery = StayRequest.query().with('student')

    // Add status filter if any
    if (status) {
      fetchQuery.orWhere({ status })
    }

    try {
      const stayRequest = await fetchQuery.paginate(page, limit)
      return response.ok(stayRequest)
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Show a list of all my stay request.
   * GET stay-requests/me
   *
   */
  async indexMe({ auth, response }) {
    const user = await auth.getUser()

    try {
      const myRequests = await user.stay_requests().fetch()
      return response.ok({ data: myRequests })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Render a form to be used for creating a new stayrequest.
   * GET stay-requests/create
   *
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new stayrequest.
   * POST stay-requests
   *
   */
  async store({ auth, request, response }) {
    const { start_date, end_date, reason, payment_method } = request.post()
    const user = await auth.getUser()

    // Check if empty
    const isNotValid = [start_date, end_date, payment_method].some((i) => i === null)
    if (isNotValid) {
      return response.badRequest({
        message: 'Please fill up required fields',
        messageCode: 'empty_fields_stay_request',
      })
    }

    // Count total days
    const total_days = moment(end_date).diff(moment(start_date), 'days') + 1

    try {
      const newRequest = await StayRequest.create({
        start_date,
        end_date,
        reason,
        payment_method,
        total_days,
        status: 'pending',
        student_id: user.id,
      })

      return response.created({
        message: 'Stay request has been made',
        messageCode: 'stay_request_created',
        data: newRequest,
      })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
        error,
      })
    }
  }

  /**
   * Display a single stayrequest.
   * GET stay-requests/:id
   *
   */
  async show({ params, response }) {
    const stayRequest = await StayRequest.query()
      .where('id', params.id)
      .with('student', (builder) => {
        builder
          .select('id', 'name', 'room_id', 'matric_no', 'course_id')
          .with('course', (builder) => builder.select('id', 'name'))
          .with('room', (builder) => builder.select('id', 'name'))
      })
      .with('respond_admin', (builder) => builder.select('id', 'name'))
      .with('paid_admin', (builder) => builder.select('id', 'name'))
      .first()
    if (!stayRequest) return response.notFound({ message: 'Stay request not found' })
    return response.ok({ data: stayRequest })
  }

  /**
   * Display a single stayrequest of mine.
   * GET stay-requests/:id/student
   *
   */
  async showMe({ auth, params, response }) {
    const user = await auth.getUser()
    const stayRequest = await StayRequest.query()
      .where('id', params.id)
      .with('respond_admin', (builder) => builder.select('id', 'name'))
      .with('paid_admin', (builder) => builder.select('id', 'name'))
      .first()
    if (!stayRequest) return response.notFound({ message: 'Stay request not found' })
    if (stayRequest.student_id !== user.id) {
      return response.badRequest({ message: "Can't view stay request that is not yours" })
    }

    return response.ok({ data: stayRequest })
  }

  /**
   * Render a form to update an existing stayrequest.
   * GET stay-requests/:id/edit
   *
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update stayrequest details.
   * PUT or PATCH stay-requests/:id
   *
   */
  async update({ auth, params, request, response }) {
    const user = await auth.getUser()
    const stayRequest = await StayRequest.find(params.id)
    const { start_date, end_date, reason, payment_method } = request.post()

    // Check if exist
    if (!stayRequest)
      return response.notFound({
        message: 'Stay request not found',
        messageCode: 'not_found_stay_request',
      })

    // Check if eligible to edit
    if (stayRequest.status !== 'pending')
      return response.badRequest({ message: "Can't edit stay request" })

    if (stayRequest.student_id !== user.id)
      return response.badRequest({ message: "Can't edit stay request that is not yours" })

    // Check if empty
    const isNotValid = [start_date, end_date, payment_method].some((i) => i === null)
    if (isNotValid) {
      return response.badRequest({
        message: 'Please fill up required fields',
        messageCode: 'empty_fields_stay_request',
      })
    }

    try {
      // Update stay request
      stayRequest.start_date = start_date
      stayRequest.end_date = end_date
      stayRequest.reason = reason
      stayRequest.payment_method = payment_method
      stayRequest.total_days = moment(end_date).diff(moment(start_date), 'days') + 1

      await stayRequest.save()

      return response.ok({
        message: 'Stay request updated',
        messageCode: 'update_stay_request_success',
        data: stayRequest,
      })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
        error,
      })
    }
  }

  /**
   * Update stayrequest status.
   * PUT or PATCH stay-requests/:id/:action
   *
   */
  async updateStatus({ auth, params, request, response }) {
    const { id, action } = params
    const { price_per_day } = request.post()
    const user = await auth.getUser()

    const stayRequest = await StayRequest.find(id)

    // Check if exist
    if (!stayRequest) {
      return response.badRequest({
        message: 'Stay request not found',
        messageCode: 'invalid_stay_request',
      })
    }

    // Check for valid action
    const validAction = ['approved', 'rejected', 'paid']
    if (!validAction.includes(action)) {
      return response.badRequest({
        message: 'Invalid action ',
        messageCode: 'invalid_action_stay_request',
      })
    }

    // Check for price_per_day for approved
    if (action === 'approved' && !price_per_day)
      return response.badRequest({
        message: 'Please provide price per day',
        messageCode: 'invalid_price_per_day',
      })

    try {
      // Update admin id for action and update date
      if (action === 'paid') {
        stayRequest.paid_admin_id = user.id
        stayRequest.paid_date = new Date()
      } else {
        stayRequest.respond_admin_id = user.id
        stayRequest.respond_date = new Date()

        if (action === 'approved') stayRequest.price_per_day = price_per_day
      }

      stayRequest.status = action
      await stayRequest.save()

      const updatedStayRequest = await StayRequest.query()
        .where({ id })
        .with('student', (builder) => {
          builder
            .select('id', 'name', 'room_id', 'matric_no', 'course_id')
            .with('course', (builder) => builder.select('id', 'name'))
            .with('room', (builder) => builder.select('id', 'name'))
        })
        .with('respond_admin', (builder) => builder.select('id', 'name'))
        .with('paid_admin', (builder) => builder.select('id', 'name'))
        .first()

      if (action !== 'paid') {
        // Fire event to notify student
        Event.fire('stay-request::update', { action, stayRequest: updatedStayRequest })
      }

      return response.ok({
        message: 'Stay request updated',
        messageCode: 'update_stay_request_success',
        data: updatedStayRequest,
      })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
        error,
      })
    }
  }

  /**
   * Delete a stayrequest with id.
   * DELETE stay-requests/:id
   *
   */
  async destroy({ auth, params, response }) {
    const user = await auth.getUser()
    const stayRequest = await StayRequest.find(params.id)

    // Check if exist
    if (!stayRequest)
      return response.notFound({
        message: 'Stay request not found',
        messageCode: 'not_found_stay_request',
      })

    // Check if eligible to delete
    if (stayRequest.status !== 'pending')
      return response.badRequest({ message: "Can't delete stay request" })

    if (stayRequest.student_id !== user.id)
      return response.badRequest({ message: "Can't delete stay request that is not yours" })

    try {
      await stayRequest.delete()
      return response.ok({ message: 'Delete success', data: stayRequest.id })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong ', error })
    }
  }

  /**
   * Make checkin/checkout request.
   * PUT or PATCH stay-requests/:id/:action/student
   *
   */
  async updateStudentStatus({ auth, params, response }) {
    const { id, action } = params
    const user = await auth.getUser()
    const stayRequest = await StayRequest.find(id)

    // Check if exist
    if (!stayRequest) {
      return response.badRequest({
        message: 'Stay request not found',
        messageCode: 'not_found_stay_request',
      })
    }

    // Check for valid action
    const validAction = ['checkin', 'checkout']
    if (!validAction.includes(action)) {
      return response.badRequest({
        message: 'Invalid action ',
        messageCode: 'invalid_action_stay_request',
      })
    }

    // Check if stay request belongs to user
    if (stayRequest.student_id !== user.id) {
      return response.badRequest({
        message: "Can't update stay request that is not yours",
        messageCode: 'forbidden_action_stay_request',
      })
    }

    // Check if valid stay request status
    if (['pending', 'rejected'].includes(stayRequest.status)) {
      return response.badRequest({
        message: `Can't ${action} stay request that is ${stayRequest.status}`,
        messageCode: 'invalid_status_stay_request',
      })
    }

    // Check checkin date if action is checkout
    if (action === 'checkout' && !stayRequest.checkin_date) {
      return response.badRequest({
        message: "Can't checkout before checkin",
        messageCode: 'invalid_student_status_stay_request',
      })
    }

    try {
      stayRequest[`${action}_date`] = new Date()
      await stayRequest.save()

      const updatedStayRequest = await StayRequest.query()
        .where({ id })
        .with('student', (builder) => {
          builder
            .select('id', 'name', 'room_id', 'matric_no', 'course_id')
            .with('course', (builder) => builder.select('id', 'name'))
            .with('room', (builder) => builder.select('id', 'name'))
        })
        .with('respond_admin', (builder) => builder.select('id', 'name'))
        .with('paid_admin', (builder) => builder.select('id', 'name'))
        .first()

      return response.ok({
        message: 'Stay request updated',
        messageCode: 'update_student_status_stay_request_success',
        data: updatedStayRequest,
      })
    } catch (error) {
      return response.internalServerError({
        message: 'Something went wrong',
        messageCode: 'internal_server_error',
        error,
      })
    }
  }
}

module.exports = StayRequestController
