'use strict'
const { validate } = use('Validator')
const Event = use('Event')
const Course = use('App/Models/Course')
class CourseController {
  /**
   * Show a list of all courses.
   * GET courses
   *
   */
  async index({ request, response }) {
    const { page, limit, q: searchQuery } = request.get()

    const fetchQuery = Course.query()

    // Add search query
    if (searchQuery) {
      const keyword = `%${searchQuery}%`

      fetchQuery.orWhere('name', 'LIKE', keyword)
      fetchQuery.orWhere('code', 'LIKE', keyword)
      fetchQuery.orWhere('description', 'LIKE', keyword)
    }

    try {
      const courses = await fetchQuery.paginate(page, limit)
      return response.ok(courses)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong' })
    }
  }

  /**
   * Create/save a new course.
   * POST courses
   *
   */
  async store({ request, response }) {
    const { name, description, code } = request.post()
    if (!name || !code) return response.badRequest({ message: 'Course name and code is required' })

    const newCourseData = { name, description, code }

    // Check if course code is unique
    const rule = { code: 'unique:courses,code' }
    const validation = await validate(newCourseData, rule)
    if (validation.fails()) {
      return response.badRequest({ message: `Course with code ${code} already exist` })
    }

    // Create course
    try {
      const newCourse = await Course.create(newCourseData)
      return response.created({ message: 'New course added', data: newCourse })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong' })
    }
  }

  /**
   * Display a single course.
   * GET courses/:id
   *
   */
  async show({ params, request, response }) {
    const { students } = request.get()
    const course = await Course.find(params.id)
    if (!course) return response.notFound({ message: 'Course not found' })

    // Include student info if required
    if (students) {
      course.students = await course.students().fetch()
    }

    return response.ok({ data: course })
  }

  /**
   * Update course details.
   * PUT or PATCH courses/:id
   *
   */
  async update({ params, request, response }) {
    // Find course
    const course = await Course.find(params.id)
    if (!course) return response.notFound({ message: 'Course not found' })

    // Check for empty field
    const { name, description, code } = request.post()
    if (!name || !code) return response.badRequest({ message: 'Course name and code is required' })

    const newCourseData = { name, description, code }

    // Check if course code is unique
    const rule = { code: 'unique:courses,code' }
    const validation = await validate(newCourseData, rule)
    if (validation.fails()) {
      return response.badRequest({ message: `Course with code ${code} already exist` })
    }

    // Assign new value
    course.name = newCourseData.name
    course.code = newCourseData.code
    course.description = newCourseData.description

    // Update course
    try {
      await course.save()
      course.students = await course.students().fetch()
      return response.ok({ message: 'Course updated', data: course })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong' })
    }
  }

  /**
   * Delete a course with id.
   * DELETE courses/:id
   *
   */
  async destroy({ params, response }) {
    const course = await Course.find(params.id)
    if (!course) return response.notFound({ message: 'Course not found' })

    // Check for associated students
    const students = await course.students().fetch()
    if (students.toJSON().length > 0)
      return response.badRequest({ message: 'Cannot delete course with registered students' })

    try {
      await course.delete()
      return response.ok({ message: 'Delete success', data: course.id })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Toggle checkin/checkout status.
   * PUT courses/:id/:action
   *
   */
  async toggle({ params, response }) {
    // Find course
    const course = await Course.find(params.id)
    if (!course) return response.notFound({ message: 'Course not found' })

    // Check action
    let isSendEmail = false
    switch (params.action) {
      case 'checkin':
        course.is_checkin = !course.is_checkin
        isSendEmail = course.is_checkin
        break
      case 'checkout':
        course.is_checkout = !course.is_checkout
        isSendEmail = course.is_checkout
        break
    }

    try {
      // Update course
      await course.save()

      // Fire course-update event if it's open
      if (isSendEmail) Event.fire('course-status::open', { course, action: params.action })

      return response.ok({ message: 'Course status updated', data: course })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong' })
    }
  }
}

module.exports = CourseController
