'use strict'

const Env = use('Env')
const Drive = use('Drive')
const path = require('path')
const StudentUpload = use('App/Models/StudentUpload')

class StudentUploadController {
  /**
   * Show a list of all student-uploads.
   * GET student-uploads/:type
   *
   */
  async index({ auth, request, response }) {
    const type = request.params.type
    const user = await auth.getUser()

    try {
      const uploads = await user.uploads().where({ type }).fetch()
      return response.ok({ data: uploads })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Get student signature
   * GET student-uploads/signature/:id
   *
   */
  async signature({ request, response }) {
    const signature = await StudentUpload.findBy({
      student_id: request.params.id,
      type: 'signature',
    })
    if (!signature) return response.notFound({ message: 'Student signature not found' })

    try {
      return response.ok({ data: signature })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Upload a merit attachment
   * POST student-uploads/:type
   *
   */
  async store({ auth, request, response }) {
    const validationOptions = { size: '10mb' }
    const type = request.params.type
    const user = await auth.getUser()

    let filePath = ''

    request.multipart.file('file', validationOptions, async (file) => {
      // Set file size
      file.size = file.stream.byteCount

      // Run validation rules
      await file.runValidations()
      const error = file.error()
      if (error.message) throw new Error(error.message)

      // Prepare for upload
      const uploadedFileName = file.clientName.split('.')[0]
      const fileName = `student_${user.id}_${uploadedFileName.split(' ').join('_')}.${file.extname}`
      filePath = path.join(type, fileName)

      // upload file to s3
      await Drive.disk('s3').put(filePath, file.stream, {
        ContentType: file.headers['content-type'],
        ACL: 'public-read',
      })
    })

    await request.multipart.process()

    // Save uploads to db
    await StudentUpload.create({
      type,
      path: filePath,
      url: Env.get('S3_URL') + filePath,
      student_id: user.id,
    })

    return response.ok({ message: 'File uploaded' })
  }

  /**
   * Delete a studentupload with id.
   * DELETE student-uploads/:id
   *
   */
  async destroy({ params, auth, response }) {
    // Get user
    const user = await auth.getUser()

    // Find uploaded file
    const upload = await user.uploads().where('id', params.id).first()
    if (!upload) return response.badRequest({ message: 'File not found' })

    // Check if attachment is related to merit
    const merits = await upload.merit().first()
    if (merits) {
      const message = `This file is being used as an attachment for a merit record, it can't be deleted`
      return response.badRequest({ message })
    }

    // Delete from S3 and db
    try {
      await upload.delete()
      await Drive.disk('s3').delete(upload.url)
      return response.ok({ message: 'File deleted', data: upload.id })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }
}

module.exports = StudentUploadController
