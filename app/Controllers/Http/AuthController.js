'use strict'

const Event = use('Event')
const Encryption = use('Encryption')

class AuthController {
  /**
   * Get user profile
   * GET auth/me
   *
   */
  async me({ response, auth }) {
    try {
      const user = await auth.getUser()
      return response.ok({ data: user })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Login user with email and password
   * POST auth/login
   *
   */
  async login({ request, response, auth }) {
    const { email, password, user_type } = request.post()

    // Validate request body
    if (!email || !password)
      return response.badRequest({ message: 'Please provide email and password' })

    // Delete previous refresh token
    const user = await use(`App/Models/${user_type}`).findBy('email', email)
    if (user) await user.tokens().where('type', 'jwt_refresh_token').delete()

    // Attempt login
    try {
      const data = await auth.authenticator(user_type).withRefreshToken().attempt(email, password)
      return response.ok({ message: 'Login success', data })
    } catch (error) {
      return response.badRequest({ message: 'Invalid email or password' })
    }
  }

  /**
   * Get new access token with refresh token
   * POST auth/refresh-token
   *
   */
  async refreshToken({ request, response, auth }) {
    try {
      const { refreshToken, user_type } = request.post()
      const accessToken = await auth.authenticator(user_type).generateForRefreshToken(refreshToken)
      return response.json({ data: accessToken })
    } catch (error) {
      return response.badRequest({ message: 'You have been logged out' })
    }
  }

  /**
   * Revoke refresh token
   * POST auth/logout
   *
   */
  async logout({ request, response }) {
    const { refreshToken, user_type } = request.post()
    const decoded = Encryption.decrypt(refreshToken)

    // Revoke refreshToken
    const token = await use(`App/Models/${user_type}Token`).findBy('token', decoded)
    if (token) token.delete()

    return response.noContent()
  }

  /**
   * Generate password reset token
   * POST auth/forgot-password
   *
   */
  async forgotPassword({ request, response }) {
    const { email, user_type } = request.post()

    // Check user input
    if (!email) return response.badRequest({ message: 'Email is required' })

    // Generate reset token
    const uuid = require('uuid')
    const token = uuid.v4()

    // Set 24 hours validity
    const expired_at = new Date(Date.now() + 86400000)

    // Find user
    const user = await use(`App/Models/${user_type}`).findBy('email', email)

    if (user) {
      // Fetch all user's token
      const tokens = await user.tokens().fetch()
      const prevToken = tokens.toJSON().find((t) => t.type === 'password')

      // Check if request has been made
      if (prevToken) {
        const isExpired = Date.now() > new Date(prevToken.expired_at)

        // Renew if expired
        if (isExpired) {
          const expiredTokenId = prevToken.id
          const expiredToken = await use(`App/Models/${user_type}Token`).find(expiredTokenId)

          // Reset expiry date and renew token
          expiredToken.expired_at = expired_at
          expiredToken.token = token
          expiredToken.save()

          Event.fire('forgot::password', { user, token, user_type })
        }
        return response.ok({ message: 'If your account exist, you should get an email.' })
      }

      // Generate token
      const generatedToken = await use(`App/Models/${user_type}Token`).create({
        expired_at,
        type: 'password',
        token,
      })
      await generatedToken.user().associate(user)
      Event.fire('forgot::password', { user, token, user_type })
    }

    return response.ok({ message: 'If your account exist, you should get an email.' })
  }

  /**
   * Update password by token
   * POST auth/update-password
   *
   */
  async updatePasswordByToken({ request, response }) {
    const { token, password, user_type } = request.post()

    // Check user input
    if (!password) return response.badRequest({ message: 'Password is required' })
    if (!token) return response.badRequest({ message: 'Invalid link. Please make a new request.' })

    // Find token
    const userToken = await use(`App/Models/${user_type}Token`).findBy('token', token)
    if (!userToken)
      return response.badRequest({ message: 'Invalid link. Please make a new request.' })

    // Check if token expired
    const isExpired = Date.now() > new Date(userToken.expired_at)
    if (isExpired)
      return response.badRequest({ message: 'Link has expired. Please make a new request.' })

    // Update password
    const user = await userToken.user().fetch()
    user.password = password
    user.save()
    userToken.delete()

    return response.ok({ message: 'Password updated! You can now login with the new password.' })
  }

  /**
   * Change password for logged in user
   * POST auth/change-password
   *
   */
  async changePassword({ request, response, auth }) {
    const { old_password, password } = request.post()
    const user = await auth.getUser()
    const isMatch = await user.verifyPassword(old_password)
    if (!isMatch) return response.badRequest({ message: 'Invalid password' })
    if (!password) return response.badRequest({ message: 'Password is required' })

    try {
      user.password = password
      await user.save()
      return response.ok({ message: 'Password updated' })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong' })
    }
  }
}

module.exports = AuthController
