'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with rooms
 */
const Room = use('App/Models/Room')
const Database = use('Database')
class RoomController {
  /**
   * Show a list of all rooms.
   * GET rooms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    const { page, limit, is_available } = request.get()

    const fetchQuery = Room.query().with('students')

    // Add is_available filter if any
    if (is_available) {
      fetchQuery.where({ is_available })
    }

    try {
      const rooms = await fetchQuery.paginate(page, limit)
      return response.ok(rooms)
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Show a list of all available rooms.
   * GET rooms/available
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async available({ response }) {
    try {
      const rooms = await Room.query().whereRaw('occupied < capacity').fetch()
      return response.ok({ data: rooms })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Render a form to be used for creating a new room.
   * GET rooms/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {}

  /**
   * Create/save a new room.
   * POST rooms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { block, number, capacity, description } = request.post()

    // Check if capacity is less than 1
    if (capacity < 1) {
      return response.badRequest({ message: 'Capacity need to be at least 1' })
    }

    // Check if required field empty
    const isEmpty = [block, number, capacity].some((i) => !i)
    if (isEmpty) {
      return response.badRequest({ message: 'Room block, number, and capacity is required' })
    }

    // Check if duplicate room
    const isDuplicate = await Room.query().where({ block, number }).first()
    if (isDuplicate) {
      const message = "Can't create duplicate room. Update capacity if need to add more occupant."
      return response.badRequest({ message })
    }

    try {
      const { id } = await Room.create({ block, number, capacity, description })
      const room = await Room.find(id)
      return response.created({ data: room, message: 'Room created' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Display a single room.
   * GET rooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {}

  /**
   * Render a form to update an existing room.
   * GET rooms/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {}

  /**
   * Update room details.
   * PUT or PATCH rooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { block, number, capacity, description } = request.post()

    // Check if capacity is less than 1
    if (capacity < 1) {
      return response.badRequest({ message: 'Capacity need to be at least 1' })
    }

    // Check if required field empty
    const isEmpty = [block, number, capacity].some((i) => !i)
    if (isEmpty) {
      return response.badRequest({ message: 'Room block, number, and capacity is required' })
    }

    // Check if room exist
    const room = await Room.find(params.id)
    if (!room) return response.notFound({ message: 'Room not found' })

    // Check if duplicate room
    const isDuplicate = await Room.query().where({ block, number }).first()
    if (isDuplicate && isDuplicate.id !== room.id) {
      return response.badRequest({ message: 'Room already exist' })
    }

    // Check if new capacity is less then current occupied
    const isCapacityValid = capacity >= room.occupied
    if (!isCapacityValid) {
      return response.badRequest({
        message: `This room has ${room.occupied} students. Capacity can't be less than that.`,
      })
    }

    // Reassign new value
    room.block = block
    room.number = number
    room.capacity = capacity
    room.description = description

    // Update database
    try {
      await room.save()
      return response.ok({ data: room, message: 'Room updated' })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }

  /**
   * Delete a room with id.
   * DELETE rooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, response }) {
    // Check if room exist
    const room = await Room.find(params.id)
    if (!room) return response.notFound({ message: 'Room not found' })

    // Delete room
    try {
      await room.delete()
      return response.ok({ message: 'Room deleted', data: room.id })
    } catch (error) {
      return response.internalServerError({ error, message: 'Something went wrong' })
    }
  }
}

module.exports = RoomController
