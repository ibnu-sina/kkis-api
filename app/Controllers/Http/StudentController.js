'use strict'

const Course = use('App/Models/Course')
const Room = use('App/Models/Room')
const Student = use('App/Models/Student')
const StudentInviteList = use('App/Models/StudentInviteList')

class StudentController {
  /**
   * Show a list of all students.
   * GET students
   *
   */
  async index({ request, response }) {
    const { q: searchQuery, attach, page, limit, room_status, room_id, course_id } = request.get()

    const fetchQuery = Student.query()

    // Attach any relationship
    if (attach) {
      if (attach.includes('course')) fetchQuery.with('course')
      if (attach.includes('room')) fetchQuery.with('room')
    }

    // Search query if any
    if (searchQuery) {
      const keyword = `%${searchQuery}%`

      fetchQuery
        .orWhere('name', 'LIKE', keyword)
        .orWhere('email', 'LIKE', keyword)
        .orWhere('matric_no', 'LIKE', keyword)
        .orWhere('phone_no', 'LIKE', keyword)
        .orWhere('ic_no', 'LIKE', keyword)
        .orWhere('religion', 'LIKE', keyword)
        .orWhere('race', 'LIKE', keyword)
        .orWhere('gender', 'LIKE', keyword)
        .orWhere('birth_date', 'LIKE', keyword)
        .orWhere('birth_place', 'LIKE', keyword)
    }

    // Add room status query
    if (room_status) {
      fetchQuery.where({ room_status })
    }

    // Add room filter
    if (room_id) {
      fetchQuery.where({ room_id })
    }

    // Add course filter
    if (course_id) {
      fetchQuery.where({ course_id })
    }

    try {
      const students = await fetchQuery.paginate(page, limit)
      return response.ok(students)
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Show my profile info
   * GET students/me
   *
   */
  async myProfile({ auth, response }) {
    try {
      const user = await auth.getUser()
      const { id, name } = await user.course().fetch()
      const room = await user.room().fetch()
      user.course = { id, name }
      user.room = room
      return response.ok({ data: user })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Create/save a new student.
   * POST students
   *
   */
  async store({ request, response, auth }) {
    const { password, confirm_password, token, room } = request.post()

    // Check if token empty
    if (!token) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if token exist
    const invited = await StudentInviteList.findBy({ token })
    if (!invited) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if token expired
    const isExpired = Date.now() > new Date(invited.expired_at)
    if (isExpired) return response.badRequest({ message: 'Invite link is not valid' })

    // Check if password match
    if (password !== confirm_password)
      return response.badRequest({ message: 'Password do not match' })

    // Create student account
    await Student.create({
      password,
      name: invited.name,
      email: invited.email,
      matric_no: invited.matric_no,
      room_id: room.id,
      room_status: 'checked_out',
      course_id: invited.course_id,
    })

    // Update room occupied value
    room.save()

    // Remove email from student invite list
    await invited.delete()

    // Attempt login
    try {
      const data = await auth
        .authenticator('Student')
        .withRefreshToken()
        .attempt(invited.email, password)
      return response.ok({ message: 'Register success', data })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Display a single student.
   * GET students/:id
   *
   */
  async show({ params, request, response }) {
    const { attach } = request.get()
    const student = await Student.find(params.id)
    if (!student) return response.notFound({ message: 'Student not found' })

    // Include relationship info if any
    if (attach) {
      const attachments = typeof attach === 'string' ? [attach] : attach
      for (const attach of attachments) {
        student[attach] = await student.load(attach)
      }
    }

    return response.ok({ data: student })
  }

  /**
   * Update student details.
   * PUT or PATCH students/:id
   *
   */
  async update({ request, response, auth }) {
    const { ic_no, religion, race, gender, birth_date, birth_place, phone_no } = request.post()

    // Get user indo
    const user = await auth.getUser()

    // Update user
    user.ic_no = ic_no ? ic_no : user.ic_no
    user.religion = religion ? religion : user.religion
    user.race = race ? race : user.race
    user.gender = gender ? gender : user.gender
    user.birth_date = birth_date ? birth_date : user.birth_date
    user.birth_place = birth_place ? birth_place : user.birth_place
    user.phone_no = phone_no ? phone_no : user.phone_no

    try {
      await user.save()
      return response.ok({ message: 'Profile updated', data: user })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }
  /**
   * Reassign student room.
   * PUT or PATCH students/:id/room
   *
   */
  async reassign({ params, request, response }) {
    const { room: new_room } = request.post()

    // Validate if student exist
    const student = await Student.find(params.id)
    if (!student) return response.notFound({ message: 'Student not found' })

    // Return error if new_room is the same as current room
    if (student.room_id === new_room.id) {
      return response.badRequest({ message: 'Cannot assign to the same room' })
    }

    // Dissociate and update old_room occupied if any
    if (student.room_id) {
      const old_room = await Room.find(student.room_id)
      await student.room().dissociate()
      await old_room.save()
    }

    try {
      // Associate and update new_room
      await student.room().associate(new_room)
      await new_room.save()
      return response.ok({ message: "Student's room updated", data: student })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Remove student from room.
   * DELETE students/:id/room
   *
   */
  async kick({ params, response }) {
    // Validate if student exist
    const student = await Student.find(params.id)
    if (!student) return response.notFound({ message: 'Student not found' })

    // Validate if student has a room
    if (!student.room_id) return response.badRequest({ message: 'Student has no room' })

    try {
      // Dissociate student room and update room's occupied
      const old_room = await Room.find(student.room_id)
      await student.room().dissociate()
      await old_room.save()

      return response.ok({
        message: `Student is removed from room ${old_room.block + old_room.number}`,
      })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Delete a student with id.
   * DELETE students/:id
   *
   */
  async destroy({ params, response }) {
    const student = await Student.find(params.id)
    if (!student) return response.notFound({ message: 'Student not found' })
    await student.delete()

    return response.ok({ message: 'Delete success', data: student.id })
  }
}

module.exports = StudentController
