'use strict'

const Merit = use('App/Models/Merit')
const StudentUpload = use('App/Models/StudentUpload')

class MeritController {
  /**
   * Show a list of all merits.
   * GET merits
   *
   */
  async index({ request, response }) {
    const { page, limit, status, q: searchQuery } = request.get()

    const fetchQuery = Merit.query().with('attachment').with('student')

    if (status) {
      fetchQuery.where('is_approved', status)
    }

    if (searchQuery) {
      const keyword = `%${searchQuery}%`

      fetchQuery.orWhere('name', 'LIKE', keyword)
      fetchQuery.orWhere('type', 'LIKE', keyword)
      fetchQuery.orWhere('term', 'LIKE', keyword)
      fetchQuery.orWhere('position', 'LIKE', keyword)
      fetchQuery.orWhere('level', 'LIKE', keyword)
      fetchQuery.orWhere('achievement', 'LIKE', keyword)
      fetchQuery.orWhere('merit', 'LIKE', keyword)
      fetchQuery.orWhere('session', 'LIKE', keyword)
    }
    try {
      const merits = await fetchQuery.paginate(page, limit)
      return response.ok(merits)
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Show a list of all my merits.
   * GET merits/me
   *
   */
  async indexMine({ auth, response }) {
    const user = await auth.getUser()
    try {
      const myMerit = await user
        .merits()
        .with('attachment', (builder) => builder.select('id', 'url'))
        .fetch()
      return response.ok({ data: myMerit })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Create/save a new merit.
   * POST merits
   *
   */
  async store({ auth, request, response }) {
    const user = await auth.getUser()
    const {
      type,
      name,
      date,
      term,
      position,
      level,
      achievement,
      merit,
      session,
      attachment_id,
    } = request.post()

    // Check if required field is complete
    const isNotValid = [type, name, merit, session].some((i) => i === null)
    if (isNotValid) return response.badRequest({ message: 'Please fill up required fields' })

    // Check for valid attachment
    if (attachment_id) {
      const upload = await StudentUpload.find(attachment_id)
      if (!upload) return response.badRequest({ message: 'Attachment not found' })
    }

    // Add to db
    try {
      const newMerit = await Merit.create({
        student_id: user.id,
        type,
        name,
        date,
        term,
        position,
        level,
        achievement,
        merit,
        session,
        attachment_id,
      })

      return response.created({ message: 'Merit sucessfully added', data: newMerit })
    } catch (error) {
      return response.badRequest({ message: 'Something went wrong', error })
    }
  }

  /**
   * Display a single merit for admin.
   * GET merits/:id/admin
   *
   */
  async show({ params, response }) {
    const merit = await Merit.query()
      .where('id', params.id)
      .with('attachment', (builder) => builder.select('id', 'url'))
      .first()
    if (!merit) return response.notFound({ message: 'Merit not found' })
    return response.ok({ data: merit })
  }

  /**
   * Display a single merit for student.
   * GET merits/:id/student
   *
   */
  async showMe({ auth, params, response }) {
    const user = await auth.getUser()
    const merit = await Merit.query()
      .where('id', params.id)
      .with('attachment', (builder) => builder.select('id', 'url'))
      .first()
    if (!merit) return response.notFound({ message: 'Merit not found' })
    if (merit.student_id !== user.id)
      return response.badRequest({ message: "Can't view merit that is not yours" })

    return response.ok({ data: merit })
  }

  /**
   * Update merit details.
   * PUT or PATCH merits/:id
   *
   */
  async update({ auth, params, request, response }) {
    // Check if can edit
    const currentMerit = await Merit.find(params.id)
    if (!currentMerit) return response.notFound({ message: 'Merit not found' })
    if (currentMerit.is_approved)
      return response.badRequest({ message: "Can't edit approved merit" })

    // Get new values
    const {
      type,
      name,
      date,
      term,
      position,
      level,
      achievement,
      merit,
      session,
      attachment_id,
    } = request.post()

    // Check if required field is complete
    const isNotValid = [type, name, merit, session].some((i) => i === null)
    if (isNotValid) return response.badRequest({ message: 'Please fill up required fields' })

    // Check for valid attachment
    if (attachment_id) {
      const upload = await StudentUpload.find(attachment_id)
      if (!upload) return response.badRequest({ message: 'Attachment not found' })
    }

    // Update merit
    currentMerit.type = type
    currentMerit.name = name
    currentMerit.date = date
    currentMerit.term = term
    currentMerit.position = position
    currentMerit.level = level
    currentMerit.achievement = achievement
    currentMerit.merit = merit
    currentMerit.session = session
    currentMerit.attachment_id = attachment_id

    try {
      await currentMerit.save()
      return response.ok({ message: 'Merit updated', data: currentMerit })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }

  /**
   * Delete a merit with id.
   * DELETE merits/:id
   *
   */
  async destroy({ auth, params, response }) {
    const user = await auth.getUser()
    const merit = await Merit.find(params.id)
    if (!merit) return response.notFound({ message: 'Merit not found' })
    if (merit.is_approved) return response.badRequest({ message: "Can't delete approved merit" })
    if (merit.student_id !== user.id)
      return response.badRequest({ message: "Can't delete merit that is not yours" })

    try {
      await merit.delete()
      return response.ok({ message: 'Delete success', data: merit.id })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong ', error })
    }
  }

  /**
   * Toggle merit approval status
   * PUT merits/:id/approve
   *
   */
  async approve({ params, response }) {
    const merit = await Merit.find(params.id)
    if (!merit) return response.notFound({ message: 'Merit not found' })

    // Toggle approval status
    merit.is_approved = !merit.is_approved

    try {
      await merit.save()
      return response.ok({ message: 'Merit status updated', data: merit })
    } catch (error) {
      return response.internalServerError({ message: 'Something went wrong', error })
    }
  }
}

module.exports = MeritController
