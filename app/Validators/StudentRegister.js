'use strict'

const { formatters } = require('indicative')

class StudentRegister {
  get rules() {
    return {
      email: 'required|email|unique:students,email',
      password: 'required',
      full_name: 'required',
      ic_no: 'required|unique:students,ic_no|min:12|max:12',
      religion: 'required',
      race: 'required',
      gender: 'required',
      birth_date: 'required|date',
      birth_place: 'required',
      matric_no: 'required|unique:students,matric_no',
      faculty: 'required',
      course: 'required',
      phone_no: 'required',
    }
  }

  get sanitizationRules() {
    return {
      full_name: 'escape',
      ic_no: 'escape',
      religion: 'escape',
      race: 'escape',
      birth_date: 'to_date',
      birth_place: 'escape',
      matric_no: 'escape',
      faculty: 'escape',
      course: 'escape',
      phone_no: 'escape',
    }
  }

  get messages() {
    return {
      'email.unique': 'Email already exist',
      'ic_no.unique': 'IC number already exist',
      'matric_no.unique': 'Matric number already exist',
    }
  }

  get validateAll() {
    return true
  }

  get formatter() {
    return formatters.vanilla
  }

  async fails(errorMessages) {
    return this.ctx.response.badRequest({ validationError: errorMessages })
  }
}

module.exports = StudentRegister
