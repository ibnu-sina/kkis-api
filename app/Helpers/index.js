'use strict'

const { validate } = use('Validator')
const uuid = require('uuid')

const generateToken = async (days = 1) => {
  const token = uuid.v4()
  const expired_at = new Date(Date.now() + 86400000 * days)
  return { token, expired_at }
}

const splitArray = async (students = []) => {
  // Add duplicate flag
  const clasify = await Promise.all(
    students.map(async (s) => {
      const rule = {
        email: 'unique:student_invite_lists,email',
        matric_no: 'unique:student_invite_lists,matric_no',
      }
      const message = {
        'email.unique': 'Email already exist',
        'matric_no.unique': 'Matric No. already exist',
      }
      const validation = await validate(s, rule, message)
      return { ...s, duplicate: validation.fails() }
    })
  )

  // Split into 2 arrays
  const noDuplicate = clasify.filter((s) => !s.duplicate)
  const withDuplicate = clasify.filter((s) => s.duplicate)

  // Remove duplicate key
  const unique = noDuplicate.map(({ duplicate, course_name, room, ...rest }) => rest)
  const duplicates = withDuplicate.map(({ duplicate, course_name, room, ...rest }) => rest)

  return { unique, duplicates, clasify }
}

const Capitalize = (string) => string.replace(/\b\w/g, (l) => l.toUpperCase())

module.exports = { generateToken, splitArray, Capitalize }
