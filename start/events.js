const Event = use('Event')

Event.on('forgot::password', 'Auth.forgotPassword')
Event.on('new::invite', 'Admin.newInvite')
Event.on('new::student-invite', 'Student.newInvite')
Event.on('course-status::open', 'Student.courseStatusOpen')
Event.on('stay-request::update', 'Student.stayRequestUpdate')
