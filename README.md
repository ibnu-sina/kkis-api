# Student Management API application

## How to run
1. Clone the project
2. Install Adonis CLI `npm i -g @adonisjs/cli`
3. Copy `.env.example` and paste it into `.env` file
4. Change `.env` file to your own setup
5. Run `npm install` to install dependencies
6. Run `adonis key:generate` to create APP_KEY in `.env` file
7. Run `adonis migration:run` to run database migration
8. Run `npm run dev` to start a development server on port 3333


Link to API documentation [here](https://documenter.getpostman.com/view/8458544/SzS7QmS5?version=latest#01e79ac5-8fc5-441e-b09c-bd219f2fccc4)